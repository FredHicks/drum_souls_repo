﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SA
{
    public class ParticleHook : MonoBehaviour
    {
        ParticleSystem[] particels;

        public void Init()
        {
            particels = GetComponentsInChildren<ParticleSystem>();
        }
    
        public void Emit(int v = 1)
        {
            if (particels == null)
                return;

            for (int i = 0; i < particels.Length; i++)
            {
                particels[i].Emit(v);
            }
        }
    }
}
