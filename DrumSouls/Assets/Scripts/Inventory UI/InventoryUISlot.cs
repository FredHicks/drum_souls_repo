﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SA.UI
{
    public class InventoryUISlot : MonoBehaviour
    {

        public Image icon;
        public Text txt1;
        public Text txt2;
    }
}
