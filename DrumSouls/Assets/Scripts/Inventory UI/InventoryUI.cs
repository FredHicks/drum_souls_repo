﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SA;

namespace SA.UI
{
    public class InventoryUI : MonoBehaviour
    {
        public EquipmentLeft eq_left;
        public CenterOverlay cen_overlay;
        public WeaponInfo weaponInfo;
        public PlayerStatus playerStatus;
        public WeaponStatus weaponStatus;
        
        public GameObject gameMenu, inventory, centerMain, centerRight, centerOverlay;
        public GameObject equipmentScreen;
        public GameObject inventoryScreen;
        public GameObject gameUI;

        List<IconBase> iconSlotsCreated = new List<IconBase>();
        public EquipmentSlotsUI equipSlotsUI;

        public Transform eqSlotsParent;
        //public List<EquipmentSlot> equipSlots = new List<EquipmentSlot>();
        EquipmentSlot[,] equipSlots;
        public Vector2 curSlotPos;
        public int curInvIndex;
        List<IconBase> curCreatedItems;
        IconBase curInvIcon;
        int prevInvIndex;
        int maxInvIndex;

        float inputT;
        bool dontMove;

        public Color unselected;
        public Color selected;
        EquipmentSlot curEqSlot;
        EquipmentSlot prevEqSlot;

        float inpTimer;
        public float inpDelay = 0.4f;

        InputUI inp;
        InventoryManager invManager;
        public bool isSwitching;

        void HandleSlotInput(InputUI inp)
        {
            if (curEqSlot == null)
                return;

            if (inp.a_input)
            {
                isSwitching = !isSwitching;

                if (isSwitching)
                {
                    ItemType t = ItemTypeFromSlotType(curEqSlot.slotType);
                    LoadCurrentItems(t);
                }
                else
                {
                    ItemType t = ItemTypeFromSlotType(curEqSlot.slotType);
                    
                    if(t == ItemType.weapon)
                    {
                        int targetIndex = curEqSlot.itemPosition;
                        bool isLeft = (curEqSlot.itemPosition > 2) ? true : false;

                        if (isLeft)
                        {
                            targetIndex -= 3;
                            invManager.lh_weapons[targetIndex] = curInvIcon.id;
                        }
                        else
                        {
                            invManager.rh_weapons[targetIndex] = curInvIcon.id;
                        }

                    }
                    else
                    {
                        invManager.consumable_items[curEqSlot.itemPosition] = curInvIcon.id;
                    }

                    LoadEquipment(invManager, true);
                }

                ChangeToSwitching();

            }

            if (inp.b_input)
            {
                isSwitching = false;
                ChangeToSwitching();
            }
        }

        ItemType ItemTypeFromSlotType(EqSlotType t)
        {
            switch (t)
            {
                case EqSlotType.weapons:
                    return ItemType.weapon;
                case EqSlotType.arrows:
                case EqSlotType.bolts:
                case EqSlotType.equipment:
                case EqSlotType.rings:
                case EqSlotType.covenant:
                default:
                    return ItemType.spell;
                case EqSlotType.consumables:
                    return ItemType.consum;
            }
        }

        void HandleSlotMovement(InputUI inp)
        {
            int x = Mathf.RoundToInt(curSlotPos.x);
            int y = Mathf.RoundToInt(curSlotPos.y);

            bool up = (inp.vertical > 0);
            bool down = (inp.vertical < 0);
            bool left = (inp.horizontal < 0);
            bool right = (inp.horizontal > 0);

            if(!up && !down && !left && !right)
            {
                inpTimer = 0;
            }
            else
            {
                inpTimer -= Time.deltaTime;
            }
            
            if (inpTimer < 0)
                inpTimer = 0;

            if (inpTimer > 0)
                return;

            if (up)
            {
                y--;
                inpTimer = inpDelay;
            }
            if (down && x == 4 && y == 2)
            {
                y = 4;
                inpTimer = inpDelay;
            }
            else if(down)
            {
                y++;
                inpTimer = inpDelay;
            }
            if (left)
            {
                x--;
                inpTimer = inpDelay;
            }
            if (right)
            {
                x++;
                inpTimer = inpDelay;
            }

            if (x > 4)
                x = 0;
            if (x < 0)
                x = 4;
            if (y > 5)
                y = 0;
            if (y < 0)
                y = 5;

            if (curEqSlot != null)
                curEqSlot.icon.background.color = unselected;

            if (x == 4 && y == 3)
                y = 2;

            curEqSlot = equipSlots[x, y];

            curSlotPos.x = x;
            curSlotPos.y = y;

            if(curEqSlot != null)
                curEqSlot.icon.background.color = selected;
        }

        void HandleInventoryMovement(InputUI inp)
        {
            bool up = (inp.vertical > 0);
            bool down = (inp.vertical < 0);
            bool left = (inp.horizontal < 0);
            bool right = (inp.horizontal > 0);

            if (!up && !down && !left && !right)
                inpTimer = 0;
            else
                inpTimer -= Time.deltaTime;
            

            if (inpTimer < 0)
                inpTimer = 0;

            if (inpTimer > 0)
                return;

            if (up)
            {
                curInvIndex -= 5;
                inpTimer = inpDelay;
            }
            if (down)
            {
                curInvIndex += 5;
                inpTimer = inpDelay;
            }
            if (left)
            {
                curInvIndex -= 1;
                inpTimer = inpDelay;
            }
            if (right)
            {
                curInvIndex += 1;
                inpTimer = inpDelay;
            }

            if(curInvIndex > maxInvIndex -1)
                curInvIndex = 0;
            if (curInvIndex < 0)
                curInvIndex = maxInvIndex - 1;
        }

        #region Init
        public void Init(InventoryManager inv)
        {
            inp = InputUI.singleton;
            invManager = inv;
            CreateUIElements();
            InitEqSlots();
        }

        void InitEqSlots()
        {
            EquipmentSlot[] eq = eqSlotsParent.GetComponentsInChildren<EquipmentSlot>();
            equipSlots = new EquipmentSlot[5, 6];

            for (int i = 0; i < eq.Length; i++)
            {
                eq[i].Init(this);
                int x = Mathf.RoundToInt(eq[i].slotPos.x);
                int y = Mathf.RoundToInt(eq[i].slotPos.y);
                equipSlots[x, y] = eq[i];
            }
        }

        void CreateUIElements()
        {
            WeaponInfoInit();
            PlayerStatusInit();
            WeaponStatusInit();
        }

        void WeaponInfoInit()
        {
            for (int i = 0; i < 6; i++)
            {
                CreateAttDefUIElement(weaponInfo.ap_slots, weaponInfo.ap_grid, (AttackDefenseType)i);
            }

            for (int i = 0; i < 5; i++)
            {
                CreateAttDefUIElement(weaponInfo.g_absorb, weaponInfo.g_abs_grid, (AttackDefenseType)i);
            }

            CreateAttDefUIElement(weaponInfo.g_absorb, weaponInfo.g_abs_grid, AttackDefenseType.stability);

            CreateAttDefUIElement_Mini(weaponInfo.a_effects, weaponInfo.a_effects_grid, AttackDefenseType.bleed);
            CreateAttDefUIElement_Mini(weaponInfo.a_effects, weaponInfo.a_effects_grid, AttackDefenseType.curse);
            CreateAttDefUIElement_Mini(weaponInfo.a_effects, weaponInfo.a_effects_grid, AttackDefenseType.frost);

            CreateAttributeElement_Mini(weaponInfo.att_bonus, weaponInfo.att_grid, AttributeType.strength);
            CreateAttributeElement_Mini(weaponInfo.att_bonus, weaponInfo.att_grid, AttributeType.dexterity);
            CreateAttributeElement_Mini(weaponInfo.att_bonus, weaponInfo.att_grid, AttributeType.intelligence);
            CreateAttributeElement_Mini(weaponInfo.att_bonus, weaponInfo.att_grid, AttributeType.faith);

            CreateAttributeElement_Mini(weaponInfo.att_reqs, weaponInfo.att_req_grid, AttributeType.strength);
            CreateAttributeElement_Mini(weaponInfo.att_reqs, weaponInfo.att_req_grid, AttributeType.dexterity);
            CreateAttributeElement_Mini(weaponInfo.att_reqs, weaponInfo.att_req_grid, AttributeType.intelligence);
            CreateAttributeElement_Mini(weaponInfo.att_reqs, weaponInfo.att_req_grid, AttributeType.faith);
        }

        void PlayerStatusInit()
        {
            CreateAttributeElement(playerStatus.attSlots, playerStatus.attGrid, AttributeType.level, "Level");

            CreateEmptySlot(playerStatus.attGrid);

            CreateAttributeElement(playerStatus.attSlots, playerStatus.attGrid, AttributeType.vigor, "Vigor");
            CreateAttributeElement(playerStatus.attSlots, playerStatus.attGrid, AttributeType.attunement, "Attunement");
            CreateAttributeElement(playerStatus.attSlots, playerStatus.attGrid, AttributeType.endurance, "Endurance");
            CreateAttributeElement(playerStatus.attSlots, playerStatus.attGrid, AttributeType.vitality, "Vitality");
            CreateAttributeElement(playerStatus.attSlots, playerStatus.attGrid, AttributeType.strength, "Strength");
            CreateAttributeElement(playerStatus.attSlots, playerStatus.attGrid, AttributeType.dexterity, "Dexterity");
            CreateAttributeElement(playerStatus.attSlots, playerStatus.attGrid, AttributeType.intelligence, "Intelligence");
            CreateAttributeElement(playerStatus.attSlots, playerStatus.attGrid, AttributeType.faith, "Faith");
            CreateAttributeElement(playerStatus.attSlots, playerStatus.attGrid, AttributeType.luck, "Luck");

            CreateEmptySlot(playerStatus.attGrid);

            CreateAttributeElement(playerStatus.attSlots, playerStatus.attGrid, AttributeType.hp, "HP");
            CreateAttributeElement(playerStatus.attSlots, playerStatus.attGrid, AttributeType.fp, "FP");
            CreateAttributeElement(playerStatus.attSlots, playerStatus.attGrid, AttributeType.stamina, "Stamina");

            CreateEmptySlot(playerStatus.attGrid);

            CreateAttributeElement(playerStatus.attSlots, playerStatus.attGrid, AttributeType.equip_load, "Equip Load");
            CreateAttributeElement(playerStatus.attSlots, playerStatus.attGrid, AttributeType.poise, "Poise");
            CreateAttributeElement(playerStatus.attSlots, playerStatus.attGrid, AttributeType.item_discovery, "Item Discovery");
            CreateAttributeElement(playerStatus.attSlots, playerStatus.attGrid, AttributeType.attunement_slots, "Attunement Slots");
        }

        void WeaponStatusInit()
        {
            CreateWeaponStatusSlot(playerStatus.defSlots, playerStatus.defGrid, AttackDefenseType.physical, "Physical");
            CreateWeaponStatusSlot(playerStatus.defSlots, playerStatus.defGrid, AttackDefenseType.strike, "VS Strike");
            CreateWeaponStatusSlot(playerStatus.defSlots, playerStatus.defGrid, AttackDefenseType.slash, "VS Slash");
            CreateWeaponStatusSlot(playerStatus.defSlots, playerStatus.defGrid, AttackDefenseType.thrust, "VS Thrust");

            CreateWeaponStatusSlot(playerStatus.defSlots, playerStatus.defGrid, AttackDefenseType.magic, "Magic");
            CreateWeaponStatusSlot(playerStatus.defSlots, playerStatus.defGrid, AttackDefenseType.fire, "Fire");
            CreateWeaponStatusSlot(playerStatus.defSlots, playerStatus.defGrid, AttackDefenseType.lightning, "Lightning");
            CreateWeaponStatusSlot(playerStatus.defSlots, playerStatus.defGrid, AttackDefenseType.dark, "Dark");

            CreateWeaponStatusSlot(playerStatus.resSlots, playerStatus.resGrid, AttackDefenseType.bleed, "Bleed");
            CreateWeaponStatusSlot(playerStatus.resSlots, playerStatus.resGrid, AttackDefenseType.poison, "Poison");
            CreateWeaponStatusSlot(playerStatus.resSlots, playerStatus.resGrid, AttackDefenseType.frost, "Frost");
            CreateWeaponStatusSlot(playerStatus.resSlots, playerStatus.resGrid, AttackDefenseType.curse, "Curse");

            CreateAttackPowerSlot(playerStatus.apSlots, playerStatus.apGrid, "R Weapon 1");
            CreateAttackPowerSlot(playerStatus.apSlots, playerStatus.apGrid, "R Weapon 2");
            CreateAttackPowerSlot(playerStatus.apSlots, playerStatus.apGrid, "R Weapon 3");

            CreateAttackPowerSlot(playerStatus.apSlots, playerStatus.apGrid, "L Weapon 1");
            CreateAttackPowerSlot(playerStatus.apSlots, playerStatus.apGrid, "L Weapon 2");
            CreateAttackPowerSlot(playerStatus.apSlots, playerStatus.apGrid, "L Weapon 3");

        }

        void CreateWeaponStatusSlot(List<PlayerStatusDef> l, Transform p, AttackDefenseType t, string txt1Text = null)
        {
            PlayerStatusDef w = new PlayerStatusDef();
            GameObject g = Instantiate(playerStatus.doubleSlot_template) as GameObject;
            g.SetActive(true);
            g.transform.SetParent(p);
            w.type = t;
            w.slot = g.GetComponent<InventoryUIDoubleSlot>();
            if (string.IsNullOrEmpty(txt1Text))
                w.slot.txt1.text = t.ToString();
            else
                w.slot.txt1.text = txt1Text;
            w.slot.txt2.text = "30";
            w.slot.txt3.text = "30";
        }

        void CreateAttackPowerSlot(List<AttackPowerSlot> l, Transform p, string id)
        {
            AttackPowerSlot a = new AttackPowerSlot();
            l.Add(a);

            GameObject g = Instantiate(weaponInfo.slot_template) as GameObject;
            g.transform.SetParent(p);
            a.slot = g.GetComponent<InventoryUISlot>();
            a.slot.txt1.text = id;
            a.slot.txt2.text = "30";
            g.SetActive(true);
        }

        void CreateAttDefUIElement(List<AttDefType> l, Transform p, AttackDefenseType t)
        {
            AttDefType a = new AttDefType();
            a.type = t;
            l.Add(a);

            GameObject g = Instantiate(weaponInfo.slot_template) as GameObject;
            g.transform.SetParent(p);
            a.slot = g.GetComponent<InventoryUISlot>();
            a.slot.txt1.text = a.type.ToString();
            a.slot.txt2.text = "30";
            g.SetActive(true);
        }

        void CreateAttDefUIElement_Mini(List<AttDefType> l, Transform p, AttackDefenseType t)
        {
            AttDefType a = new AttDefType();
            a.type = t;
            l.Add(a);

            GameObject g = Instantiate(weaponInfo.slot_mini) as GameObject;
            g.transform.SetParent(p);
            a.slot = g.GetComponent<InventoryUISlot>();
            a.slot.txt1.text = "0";
            g.SetActive(true);
        }

        void CreateAttributeElement_Mini(List<AttributeSlot> l, Transform p, AttributeType t)
        {
            AttributeSlot a = new AttributeSlot();
            a.type = t;
            l.Add(a);

            GameObject g = Instantiate(weaponInfo.slot_mini) as GameObject;
            g.transform.SetParent(p);
            a.slot = g.GetComponent<InventoryUISlot>();
            a.slot.txt1.text = "-";
            g.SetActive(true);
        }

        void CreateAttributeElement(List<AttributeSlot> l, Transform p, AttributeType t, string txt1Text = null)
        {
            AttributeSlot a = new AttributeSlot();
            a.type = t;
            l.Add(a);

            GameObject g = Instantiate(playerStatus.slot_template) as GameObject;
            g.transform.SetParent(p);
            a.slot = g.GetComponent<InventoryUISlot>();
            if (string.IsNullOrEmpty(txt1Text))
                a.slot.txt1.text = t.ToString();
            else
                a.slot.txt1.text = txt1Text;
            a.slot.txt2.text = "30";
            g.SetActive(true);
        }
        
        void CreateEmptySlot(Transform p)
        {
            GameObject g = Instantiate(playerStatus.emptySlot) as GameObject;
            g.transform.SetParent(p);
            g.SetActive(true);
        }
        #endregion

        public UIState curState;
        
        public void LoadCurrentItems(ItemType t)
        {
            List<Item> itemList = SessionManager.singleton.GetItemsAsList(t);

            if (itemList == null)
                return;
            if (itemList.Count == 0)
                return;

            GameObject prefab = eq_left.inventory.slotTemplate;
            Transform p = eq_left.inventory.slotGrid;

            int dif = iconSlotsCreated.Count - itemList.Count;
            int extra = (dif > 0) ? dif : 0;

            maxInvIndex = itemList.Count;

            curCreatedItems = new List<IconBase>();
            curInvIndex = 0;

            for (int i = 0; i < itemList.Count + extra; i++)
            {
                if(i > itemList.Count -1)
                {
                    iconSlotsCreated[i].gameObject.SetActive(false);
                    continue;
                }

                IconBase icon = null;
                if (iconSlotsCreated.Count -1 < i)
                {
                    GameObject g = Instantiate(prefab) as GameObject;
                    g.SetActive(true);
                    g.transform.SetParent(p);
                    icon = g.GetComponent<IconBase>();
                    iconSlotsCreated.Add(icon);
                }
                else
                {
                    icon = iconSlotsCreated[i];
                }

                curCreatedItems.Add(icon);
                icon.gameObject.SetActive(true);
                icon.icon.enabled = true;
                icon.icon.sprite = itemList[i].icon;
                icon.id = itemList[i].item_id;
            }

            
        }

        void HandleUIState(InputUI inp)
        {
            switch (curState)
            {
                case UIState.equipment:
                    if (!isSwitching)
                        HandleSlotMovement(inp);
                    else
                        HandleInventoryMovement(inp);
                    HandleSlotInput(inp);
                    break;
                case UIState.inventory:
                    HandleInventoryMovement(inp);
                    break;
                case UIState.attributes:
                    break;
                case UIState.messages:
                    break;
                case UIState.options:
                    break;
            }
        }

        void ChangeToSwitching()
        {
            if (isSwitching)
            {
                equipmentScreen.SetActive(false);
                inventoryScreen.SetActive(true);
            }
            else
            {
                equipmentScreen.SetActive(true);
                inventoryScreen.SetActive(false);
            }
        }

        public void OpenUI()
        {
            LoadEquipment(invManager);
            gameMenu.SetActive(false);
            inventory.SetActive(true);
            gameUI.SetActive(false);
            prevEqSlot = null;
            curInvIndex = 0;
            prevInvIndex = -1;
        }

        public void CloseUI()
        {
            gameMenu.SetActive(false);
            inventory.SetActive(false);
            gameUI.SetActive(true);
            prevEqSlot = null;
            curInvIndex = 0;
            prevInvIndex = -1;
        } 

        public void Tick()
        {
            inp.Tick();
            HandleUIState(inp);
            
            if (prevEqSlot != curEqSlot)
            {
                if (curEqSlot != null)
                {
                    eq_left.slotName.text = curEqSlot.slotName;
                    LoadItemFromSlot(curEqSlot.icon);
                }
            }

            if (curCreatedItems != null)
            {
                if (curCreatedItems.Count > 0)
                {
                    if (prevInvIndex != curInvIndex)
                    {
                        if (curInvIcon)
                            curInvIcon.background.color = unselected;

                        if (curInvIndex < curCreatedItems.Count)
                        {
                            curInvIcon = curCreatedItems[curInvIndex];
                            curInvIcon.background.color = selected;
                            LoadItemFromSlot(curInvIcon);
                        }
                    }
                }
            }

            prevEqSlot = curEqSlot;
            prevInvIndex = curInvIndex;
        }

        public void LoadEquipment(InventoryManager inv, bool loadOnCharacter = false)
        {
            for (int i = 0; i < inv.rh_weapons.Count; i++)
            {
                if(i > 2)
                    break;
                EquipmentSlot slot = equipSlotsUI.weaponSlots[i];
                equipSlotsUI.UpdateEqSlot(inv.rh_weapons[i], slot, ItemType.weapon);
                slot.itemPosition = i;
            }

            for (int i = 0; i < inv.lh_weapons.Count; i++)
            {
                if (i > 2)
                    break;
                EquipmentSlot slot = equipSlotsUI.weaponSlots[i+3];
                equipSlotsUI.UpdateEqSlot(inv.lh_weapons[i], slot, ItemType.weapon);
                slot.itemPosition = i + 3;
            }

            for (int i = 0; i < inv.consumable_items.Count; i++)
            {
                if (i > 9)
                    break;
                
                EquipmentSlot slot = equipSlotsUI.cons[i];
                equipSlotsUI.UpdateEqSlot(inv.consumable_items[i], slot, ItemType.consum);
                slot.itemPosition = i;
            }

            if (loadOnCharacter)
            {
                invManager.LoadInventory();
            }
        }
        
        void LoadItemFromSlot(IconBase icon)
        {
            if (string.IsNullOrEmpty(icon.id))
            {
                icon.id = "unarmed";
            }

            // 
            ResourcesManager rm = ResourcesManager.singleton;

            

            switch (curEqSlot.slotType)
            {
                case EqSlotType.weapons:
                    LoadWeaponItem(rm, icon);
                    break;
                case EqSlotType.arrows:
                    break;
                case EqSlotType.bolts:
                    break;
                case EqSlotType.equipment:
                    break;
                case EqSlotType.rings:
                    break;
                case EqSlotType.covenant:
                    break;
                case EqSlotType.consumables:
                    LoadConsumableItem(rm);
                    break;
                default:
                    break;
            }
        }

        void LoadWeaponItem(ResourcesManager rm, IconBase icon)
        {
            string weaponId = icon.id;
            WeaponStats stats = rm.GetWeaponStats(weaponId);
            Item item = rm.GetItem(icon.id, ItemType.weapon);
            eq_left.curItem.text = item.name_item;
            

            UpdateCenterOverlay(item);
       
            // Center
            weaponInfo.smallIcon.sprite = item.icon;
            weaponInfo.itemName.text = item.name_item;
            weaponInfo.weaponType.text = stats.weaponType;
            weaponInfo.damageType.text = stats.skillName;
            weaponInfo.weightCost.text = stats.weightCost.ToString();
            weaponInfo.durability_min.text = stats.maxDurability.ToString();      // <<=== CHANGE THIS LATER
            weaponInfo.durability_max.text = stats.maxDurability.ToString();

            cen_overlay.skillName.text = stats.skillName;

            UpdateUIAttackElement(AttackDefenseType.physical,weaponInfo.ap_slots, stats.a_physical.ToString());
            UpdateUIAttackElement(AttackDefenseType.magic, weaponInfo.ap_slots, stats.a_magic.ToString());
            UpdateUIAttackElement(AttackDefenseType.fire, weaponInfo.ap_slots, stats.a_fire.ToString());
            UpdateUIAttackElement(AttackDefenseType.lightning, weaponInfo.ap_slots, stats.a_lightning.ToString());
            UpdateUIAttackElement(AttackDefenseType.dark, weaponInfo.ap_slots, stats.a_dark.ToString());
            UpdateUIAttackElement(AttackDefenseType.critical, weaponInfo.ap_slots, stats.a_critical.ToString());

            UpdateUIAttackElement(AttackDefenseType.frost, weaponInfo.a_effects, stats.a_frost.ToString(), true);
            UpdateUIAttackElement(AttackDefenseType.curse, weaponInfo.a_effects, stats.a_curse.ToString(), true);
            UpdateUIAttackElement(AttackDefenseType.bleed, weaponInfo.a_effects, stats.a_bleed.ToString(), true);

            UpdateUIAttackElement(AttackDefenseType.physical, weaponInfo.g_absorb, stats.d_physical.ToString());
            UpdateUIAttackElement(AttackDefenseType.magic, weaponInfo.g_absorb, stats.d_magic.ToString());
            UpdateUIAttackElement(AttackDefenseType.fire, weaponInfo.g_absorb, stats.d_fire.ToString());
            UpdateUIAttackElement(AttackDefenseType.lightning, weaponInfo.g_absorb, stats.d_lightning.ToString());
            UpdateUIAttackElement(AttackDefenseType.dark, weaponInfo.g_absorb, stats.d_dark.ToString());
            UpdateUIAttackElement(AttackDefenseType.stability, weaponInfo.g_absorb, stats.stability.ToString());
        }

        void UpdateUIAttackElement(AttackDefenseType t, List<AttDefType> l, string value, bool onTxt1 = false)
        {
            AttDefType s1 = weaponInfo.GetAttDefSlot(l, t);
            if (!onTxt1)
                s1.slot.txt2.text = value;
            else
                s1.slot.txt1.text = value;

        }

        void UpdateCenterOverlay(Item item)
        {
            cen_overlay.bigIcon.sprite = item.icon;
            cen_overlay.itemName.text = item.name_item;
            cen_overlay.itemDescription.text = item.itemDescription;
            cen_overlay.skillDescription.text = item.skillDescription;
        }

        void LoadConsumableItem(ResourcesManager rm)
        {
            string weaponId = curEqSlot.icon.id;
            Item item = rm.GetItem(curEqSlot.icon.id, ItemType.consum);

            UpdateCenterOverlay(item);
        }

        public static InventoryUI singleton;
        private void Awake()
        {
            singleton = this;
        }
    }

    public enum EqSlotType
    {
        weapons, arrows, bolts, equipment, rings, covenant, consumables 
    }

    public enum UIState
    {
        equipment, inventory, attributes, messages, options
    }

    [System.Serializable]
    public class EquipmentSlotsUI
    {
        public List<EquipmentSlot> weaponSlots = new List<EquipmentSlot>();
        public List<EquipmentSlot> arrow = new List<EquipmentSlot>();
        public List<EquipmentSlot> equipment = new List<EquipmentSlot>();
        public List<EquipmentSlot> ring = new List<EquipmentSlot>();
        public List<EquipmentSlot> cons = new List<EquipmentSlot>();
        public EquipmentSlot covenant;

        public void UpdateEqSlot(string itemId, EquipmentSlot s, ItemType itemType)
        {
            Item item = ResourcesManager.singleton.GetItem(itemId, itemType);
            s.icon.icon.sprite = item.icon;
            s.icon.icon.enabled = true;
            s.icon.id = item.item_id;
        }

        public void AddSlotOnList(EquipmentSlot eq)
        {
            switch (eq.slotType)
            {
                case EqSlotType.weapons:
                    weaponSlots.Add(eq);
                    break;
                case EqSlotType.arrows:
                case EqSlotType.bolts:
                    arrow.Add(eq);
                    break;
                case EqSlotType.equipment:
                    equipment.Add(eq);
                    break;
                case EqSlotType.rings:
                    ring.Add(eq);
                    break;
                case EqSlotType.covenant:
                    covenant = eq;
                    break;
                case EqSlotType.consumables:
                    cons.Add(eq);
                    break;
                default:
                    break;
            }
        }
    }

    [System.Serializable]
    public class EquipmentLeft
    {
        public Text slotName;
        public Text curItem;
        public Left_Inventory inventory;
    }
    

    [System.Serializable]
    public class PlayerStatus
    {
        public GameObject slot_template;
        public GameObject doubleSlot_template;
        public GameObject emptySlot;
        public Transform attGrid;
        public Transform apGrid;
        public Transform defGrid;
        public Transform resGrid;
        public List<AttributeSlot> attSlots = new List<AttributeSlot>();
        public List<AttackPowerSlot> apSlots = new List<AttackPowerSlot>();
        public List<PlayerStatusDef> defSlots = new List<PlayerStatusDef>();
        public List<PlayerStatusDef> resSlots = new List<PlayerStatusDef>();
    }

    [System.Serializable]
    public class WeaponStatus            // TODO:  Finish off the weapon Status page
    {
        public Transform ap_grid;
        public Transform defAbs_grid;
        public Transform res_grid;
        public List<AttributeSlot> ap_slots = new List<AttributeSlot>();

    }

    [System.Serializable]
    public class Left_Inventory
    {
        public Slider invSlider;
        public GameObject slotTemplate;
        public Transform slotGrid;
    }

    [System.Serializable]
    public class CenterOverlay
    {
        public Image bigIcon;
        public Text itemName;
        public Text itemDescription;
        public Text skillName;
        public Text skillDescription;
    }
    
    [System.Serializable]
    public class WeaponInfo
    {
        public Image smallIcon;
        public GameObject slot_template;
        public GameObject slot_mini;
        public GameObject breakSlot;
        public Text itemName;
        public Text weaponType;
        public Text damageType;
        public Text skillName;
        public Text fpCost;
        public Text weightCost;
        public Text durability_min;
        public Text durability_max;

        public Transform ap_grid;
        public List<AttDefType> ap_slots = new List<AttDefType>();
        public Transform g_abs_grid;
        public List<AttDefType> g_absorb = new List<AttDefType>();
        public Transform a_effects_grid;
        public List<AttDefType> a_effects = new List<AttDefType>();
        public Transform att_grid;
        public List<AttributeSlot> att_bonus = new List<AttributeSlot>();
        public Transform att_req_grid;
        public List<AttributeSlot> att_reqs = new List<AttributeSlot>();

        public AttributeSlot GetAttSlot(List<AttributeSlot> l, AttributeType type)
        {
            for (int i = 0; i < l.Count; i++)
            {
                if (l[i].type == type)
                    return l[i];
            }

            return null;
        }

        public AttDefType GetAttDefSlot(List<AttDefType> l, AttackDefenseType type)
        {
            for (int i = 0; i < l.Count; i++)
            {
                if (l[i].type == type)
                    return l[i];
            }

            return null;
        }
    }

    [System.Serializable]
    public class ItemDetails
    {

    }

    [System.Serializable]
    public class AttributeSlot
    {
        public bool isBreak;
        public AttributeType type;
        public InventoryUISlot slot;
    }

    [System.Serializable]
    public class AttDefType
    {
        public bool isBreak;
        public AttackDefenseType type;
        public InventoryUISlot slot;
    }

    [System.Serializable]
    public class AttackPowerSlot
    {
        public InventoryUISlot slot;
    }

    [System.Serializable]
    public class PlayerStatusDef
    {
        public AttackDefenseType type;
        public InventoryUIDoubleSlot slot;
    }
}
