﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SA
{
    public static class StatsCalculations
    {

       public static int CalculateBaseDamage(WeaponStats w, CharacterStats st, float mulitplier = 1)
        {
            float physical = (w.a_physical * mulitplier) - st.physical;
            float strike = (w.a_strike * mulitplier) - st.vs_strike;
            float slash = (w.a_slash * mulitplier) - st.vs_slash;
            float thrust = (w.a_thrust * mulitplier) - st.vs_thrust;

            float sum = physical + strike + slash + thrust;

            float magic = (w.a_magic * mulitplier) - st.magic;
            float fire = (w.a_fire * mulitplier) - st.fire;
            float lightning = (w.a_lightning * mulitplier) - st.lightning;
            float dark = (w.a_dark * mulitplier) - st.dark;

            sum += magic + fire + lightning + dark;

            if (sum <= 0)
                sum = 1;

            return Mathf.RoundToInt(sum);
        }
    }
}
